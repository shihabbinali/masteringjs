/**
 * Created by mizan on 16/02/18.
 */



var x = 10;
{
    console.log(x);
    var x = 20;
    console.log(x);
}

var x = "mizan";

console.log("After the block "+x);

let y = 30;
console.log("Y before block "+y);
{
    //console.log("Y is "+y);
    y = 50;
    console.log("Y is "+y);
}

console.log("Y after block: "+y);

const PI = 3.14;

var initialBalance = 200;
var interestRate = .05;

if(true){
    var year = 20;
    var total = (initialBalance + interestRate) * year;
}

var t = ( x + y ) * z; // t = total , x = initial balance , y = interest rate

var Y = 10;
var Z = 20;

var snake_case = "";
var camelCase = "";
var UpperCamelCase = "";
'this-is-kebab-case';

var X, Y;
var Z = 10;
X = 10;

let abc = 10;
var abc = 20;

console.log("abc is "+abc);

var X = 10;
let X = 20;

console.log("X is "+X);